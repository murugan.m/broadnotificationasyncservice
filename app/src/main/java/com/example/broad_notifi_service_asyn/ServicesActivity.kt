package com.example.broad_notifi_service_asyn

import android.app.ActivityManager
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.broad_notifi_service_asyn.databinding.ActivityMainBinding
import com.example.broad_notifi_service_asyn.databinding.ActivityServicesBinding

class ServicesActivity : AppCompatActivity() {
    lateinit var binding: ActivityServicesBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityServicesBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.backgroundServiceBtn.setOnClickListener {
            val intent = Intent(this, MyBackgroundService::class.java)
            startService(intent)
        }

        binding.foregroundServiceBtn.setOnClickListener {

            val intent = Intent(this, MyForeGroundService::class.java)

            if (foreGroundServiceIsRunning()) {
                Log.e(TAG, "Already Running Foreground task", )
                stopService(intent)
            }
            startForegroundService(intent)
        }
    }

    fun foreGroundServiceIsRunning() : Boolean {
        val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val activityvalues = activityManager.getRunningServices(Integer.MAX_VALUE)

        for (actions in activityvalues) {
            if (MyForeGroundService::class.java.name.equals(actions.service.className)) {
                return  true
            }
        }
        return  false
    }
}