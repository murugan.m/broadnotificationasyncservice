package com.example.broad_notifi_service_asyn

import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class BroadCastReceiver : AppCompatActivity() {
    lateinit var br: InternalBroadCastReceiver
    lateinit var broadCastStatusLbl: TextView
    var batteryStatus: Intent? = null
    var isRegister = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_broad_cast_receiver)
        title = "Broadcast Receiver"

        broadCastStatusLbl = findViewById(R.id.statusLbl)

        batteryStatus = IntentFilter(Intent.ACTION_BATTERY_CHANGED).let { ifilter ->
            broadCastStatusLbl.text = "Done"
            this.registerReceiver(null, ifilter)
        }

        val batteryPct: Float? = batteryStatus?.let { intent ->
            val level: Int = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
            val scale: Int = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
            level * 100 / scale.toFloat()
        }

        println("Battery Percentage : $batteryPct")

        val broadCaseActionBtnAction : Button = findViewById(R.id.sendBroadcastBtnAction)
        val registerBroadCastBtnAction : Button = findViewById(R.id.startServiceBtnAction)
        val stopBroadCastBtnAction : Button = findViewById(R.id.stopServiceBtnAction)

        broadCaseActionBtnAction.setOnClickListener {
            val intent = Intent("mySample_Receiver.MY_ACTION")
            intent.putExtra("data", "Battery Percentage is $batteryPct \nCurrent Charge Status ${getChargeType()}")
            sendBroadcast(intent)
            broadCastStatusLbl.text = if (isRegister) "Sent" else "Not Registered Please Register the BroadCast"
        }

        registerBroadCastBtnAction.setOnClickListener {
            startReceiver()
            isRegister = true
        }

        stopBroadCastBtnAction.setOnClickListener {
            stopReceiver()
            isRegister = false
        }
    }

    fun startReceiver() {
        br = InternalBroadCastReceiver()
        val filterVal = IntentFilter("mySample_Receiver.MY_ACTION").also {
            registerReceiver(br, it)
            broadCastStatusLbl.text = "Registered"
        }
    }

    fun stopReceiver() {
        unregisterReceiver(br)
        broadCastStatusLbl.text = "UnRegistered"
    }

    fun getChargeType() : String {
        val status: Int = batteryStatus?.getIntExtra(BatteryManager.EXTRA_STATUS, -1) ?: -1
        val isCharging: Boolean = status == BatteryManager.BATTERY_STATUS_CHARGING
                || status == BatteryManager.BATTERY_STATUS_FULL

// How are we charging?
        val chargePlug: Int = batteryStatus?.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1) ?: -1
        val usbCharge: Boolean = chargePlug == BatteryManager.BATTERY_PLUGGED_USB
        val acCharge: Boolean = chargePlug == BatteryManager.BATTERY_PLUGGED_AC

        var changeType : String = ""
        if (usbCharge) {
            changeType = "USB"
        } else {
            if (acCharge) {
                changeType = "AC"
            } else {
                changeType = "Not Charging"
            }
        }

        if (!isCharging) {
            changeType = "Not Charging"
        }
        return  changeType
    }
}