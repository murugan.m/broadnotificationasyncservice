package com.example.broad_notifi_service_asyn

import android.app.AlarmManager
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import android.service.controls.ControlsProviderService.TAG
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.core.app.NotificationCompat
import java.util.*
import kotlin.concurrent.thread
import kotlin.math.log

class MyBackgroundService : Service() {

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        Thread {
            kotlin.run {
                while (true) {
                    val date = Date()
                    val dateFormat = android.text.format.DateFormat.getLongDateFormat(applicationContext)
                    val timeFormat = android.text.format.DateFormat.getTimeFormat(applicationContext)

                    val intent = Intent(applicationContext,NotificationReceiver::class.java)
                    val descriptionVal = dateFormat.format(date) + " " + timeFormat.format(date)
                    val title = "Current Time"
                    intent.putExtra(titleVal, title)
                    intent.putExtra(messageVal, descriptionVal)

                    val notfication = NotificationCompat.Builder(MainActivity@this, channelID)
                        .setSmallIcon(R.drawable.icon_notifi)
                        .setContentTitle(intent.getStringExtra(titleVal))
                        .setContentText(intent.getStringExtra(messageVal))
                        .build()
                    val context = MainActivity@this
                    val manger = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    manger.notify(notifiationID,notfication)

                    Log.e(TAG, "Service Running In Background")
                    Thread.sleep(60000)
                }
            }
        }.start()
        return super.onStartCommand(intent, flags, startId)
    }
    override fun onBind(intent: Intent): IBinder {
        TODO("Return the communication channel to the service.")
    }
}