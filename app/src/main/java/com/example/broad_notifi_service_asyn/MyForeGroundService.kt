package com.example.broad_notifi_service_asyn

import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import android.service.controls.ControlsProviderService
import android.util.Log
import androidx.core.app.NotificationCompat
import java.text.DateFormat
import java.util.*

class MyForeGroundService : Service() {
    var descriptionVal : String = "Running"
    var title : String = "Foreground task"
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        Thread {
            kotlin.run {
                while (true) {
                    val date = Date()
                    val dateFormat = android.text.format.DateFormat.getLongDateFormat(applicationContext)
                    val timeFormat = android.text.format.DateFormat.getTimeFormat(applicationContext)
                    descriptionVal = dateFormat.format(date) + " " + timeFormat.format(date)
                    title = "Current Time"
                    val notfication = NotificationCompat.Builder(MainActivity@this, channelID)
                        .setSmallIcon(R.drawable.icon_notifi)
                        .setContentTitle(title)
                        .setContentText(descriptionVal)
                        .build()
                    val context = MainActivity@this
                    val manger = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    manger.notify(notifiationID,notfication)
                    Log.e(ControlsProviderService.TAG, "Service Running In Background")
                    Thread.sleep(30000)
                }
            }
        }.start()

        val notfication = NotificationCompat.Builder(MainActivity@this, channelID)
            .setSmallIcon(R.drawable.icon_notifi)
            .setContentTitle(title)
            .setContentText(descriptionVal)
            .build()
        startForeground(notifiationID,notfication)
        return super.onStartCommand(intent, flags, startId)
    }
    override fun onBind(intent: Intent): IBinder {
        TODO("Return the communication channel to the service.")
    }

    fun formatDurationTime(durationSeconds: Long): String {
        var hours = 0L
        var minutes = 0L
        var seconds = durationSeconds
        if (seconds >= 3600) {
            hours = seconds / 3600
            seconds -= hours * 3600
        }
        if (seconds >= 60) {
            minutes = seconds / 60
            seconds -= minutes * 60
        }
        return Formatter().format("%1\$02d:%2\$02d:%3\$02d", hours, minutes, seconds).toString()
    }
}