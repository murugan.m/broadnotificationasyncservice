package com.example.broad_notifi_service_asyn

import android.app.Notification
import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat

const val notifiationID = 1232
const val channelID = "myChannel"
const val titleVal = "titleValue"
const val messageVal = "messageValue"

class NotificationReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        // This method is called when the BroadcastReceiver is receiving an Intent broadcast.

        val notfication = NotificationCompat.Builder(context, channelID)
            .setSmallIcon(R.drawable.icon_notifi)
            .setContentTitle(intent.getStringExtra(titleVal))
            .setContentText(intent.getStringExtra(messageVal))
            .build()

        val manger = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manger.notify(notifiationID,notfication)

    }
}