package com.example.broad_notifi_service_asyn

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Message
import android.text.format.DateFormat.getLongDateFormat
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.broad_notifi_service_asyn.databinding.ActivityNotificationBinding
import java.text.DateFormat
import java.util.*

class NotificationActivity : AppCompatActivity() {
    lateinit var notificationManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var builder: Notification.Builder
    private val channelId = "i.apps.notifications"
    private val description = "Test notification"

    private  lateinit var binding : ActivityNotificationBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNotificationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val btn: Button = binding.btn

        createNotificationChannel()

        binding.btn.setOnClickListener {
            scheduleNotificaiton()
        }

    }

    private fun scheduleNotificaiton() {
        val intent = Intent(applicationContext,NotificationReceiver::class.java)
        val title = binding.titleId.text.toString()
        val descriptionVal = binding.descriptionId.text.toString()
        intent.putExtra(titleVal, title)
        intent.putExtra(messageVal, descriptionVal)

        val pendingIntent = PendingIntent.getBroadcast(applicationContext, notifiationID, intent, PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT)

        val alarmManger = getSystemService(ALARM_SERVICE) as AlarmManager
        val time = getTime()
        alarmManger.setExactAndAllowWhileIdle(
            AlarmManager.RTC_WAKEUP, time, pendingIntent
        )

        showAlert(time, title, descriptionVal)
        
    }

    private fun showAlert(time: Long, title: String, descriptionVal: String) {

        val date = Date(time)
        val dateFormat = android.text.format.DateFormat.getLongDateFormat(applicationContext)
        val timeFormat = android.text.format.DateFormat.getTimeFormat(applicationContext)

        AlertDialog.Builder(this)
            .setTitle("Notification Scheduled")
            .setMessage("Title: " + title + "\nMessage: " + descriptionVal + "\nAt: " + dateFormat.format(date) + " " + timeFormat.format(date))
            .setPositiveButton("Ok") { _,_ ->
                println("Ok")
            }.show()

    }


    private fun getTime(): Long {
        val minute = binding.timePickerId.minute
        val hour = binding.timePickerId.hour
        val day = binding.datePickerId.dayOfMonth
        val year = binding.datePickerId.year
        val month = binding.datePickerId.month

        val  calender = Calendar.getInstance()
        calender.set(year,month,day,hour,minute)
        return  calender.timeInMillis
    }

    private fun createNotificationChannel() {
        val name = "notify Channel"
        val desc = "Test notification Description"
        val  importance = NotificationManager.IMPORTANCE_DEFAULT
        val channel = NotificationChannel(channelID, name, importance)
        channel.description = desc
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }

}