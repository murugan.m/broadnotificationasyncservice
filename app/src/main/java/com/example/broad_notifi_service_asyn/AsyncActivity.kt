package com.example.broad_notifi_service_asyn

import android.content.ContentValues.TAG
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.broad_notifi_service_asyn.databinding.ActivityAsyncBinding
import com.example.broad_notifi_service_asyn.databinding.ActivityServicesBinding
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

private lateinit var binding: ActivityAsyncBinding
private var i = 1

class AsyncActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAsyncBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.button.setOnClickListener {
            var  task:MyAsyncTask = MyAsyncTask()
            task.execute()
            i += 1
        }

    }

    class MyAsyncTask : AsyncTask<String, Int, Int>() {

        override fun doInBackground(vararg p0: String?): Int {

            var inputStream: InputStream? = null

            var urlConnection: HttpURLConnection? = null

            /* forming th java.net.URL object */
            val url = URL("https://jsonplaceholder.typicode.com/todos/$i")

            urlConnection = url.openConnection() as HttpURLConnection

            /* optional request header */
            urlConnection.setRequestProperty("Content-Type", "application/json")

            /* optional request header */
            urlConnection.setRequestProperty("Accept", "application/json")

            /* for Get request */
            urlConnection.requestMethod = "GET"

            val statusCode = urlConnection.responseCode

            /* 200 represents HTTP OK */
            if (statusCode == 200) {
                inputStream = BufferedInputStream(urlConnection.inputStream)

                val response = convertInputStreamToString(inputStream)
                parseResult(response)
                return 1
            } else {
                throw IOException()
            }

        }

        @Throws(IOException::class)
        private fun convertInputStreamToString(inputStream: InputStream?): String {

            val bufferedReader = BufferedReader(InputStreamReader(inputStream!!))

            val result = inputStream.bufferedReader().use(BufferedReader::readText)

            /* Close Stream */
            inputStream.close()

            return result
        }

        private fun parseResult(result: String) {
            val response = JSONObject(result)

            val posts = response.optString("title")
            println(response)
            Log.e(TAG, "parseResult: $response")
            binding.textView.text = "parseResult: ${response}"
        }


    }
}


