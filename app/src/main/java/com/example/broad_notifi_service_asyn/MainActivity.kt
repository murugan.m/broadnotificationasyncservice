package com.example.broad_notifi_service_asyn

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.broad_notifi_service_asyn.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)
        title = "Assignment 5"

        val broadCastReceiverBtn : Button = findViewById(R.id.broadCastBtnAction)

        broadCastReceiverBtn.setOnClickListener {
        val intent = Intent(this, BroadCastReceiver::class.java)
            startActivity(intent)
        }

        val notificationBtn : Button = findViewById(R.id.notificationBtnAction)

        notificationBtn.setOnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
        }

        binding.serviceBtnAction.setOnClickListener {
            val intent = Intent(this, ServicesActivity::class.java)
            startActivity(intent)
        }


        binding.asyncBtnAction.setOnClickListener {
            val intent = Intent(this, AsyncActivity::class.java)
            startActivity(intent)
        }
    }
}

