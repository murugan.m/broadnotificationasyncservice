package com.example.broad_notifi_service_asyn

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.BatteryManager
import android.widget.Toast


class InternalBroadCastReceiver : BroadcastReceiver() {

    private  val myAction: String = "mySample_Receiver.MY_ACTION"
    override fun onReceive(context: Context, intent: Intent) {
        // This method is called when the BroadcastReceiver is receiving an Intent broadcast.
//        Log.i("InternalBroadCastReceiver", "onReceive $intent")
        val interName = intent.action
        if(interName == myAction) {
            val s = intent.extras?.getString("data")
            val setVales :CharSequence = "$s"
            Toast.makeText(context, setVales, Toast.LENGTH_SHORT).show()
        } else if (interName == Intent.ACTION_BATTERY_CHANGED) {
            val status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1)

            Toast.makeText(context, status, Toast.LENGTH_SHORT).show()
        } else if ("android.intent.action.BOOT_COMPLETED" == interName){

        }
    }
}